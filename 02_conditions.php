<?php
/**
 * Created by PhpStorm.
 * User: krouma
 * Date: 28.3.19
 * Time: 15:56
 */

$a = 10;
if ($a > 1)
{
    echo '10 > 1';
}
elseif ($a == 1)
{
    echo '10 = 1';
}
else
{
    echo '10 < 1';
}

if ('9' == 9)
{
    echo 'Tento řetězec se vypíše';
}

if ('9' === 9)
{
    echo 'Tento řetězec se nevypíše';
}

if ('Řetězec')
{
    echo 'Nenulový řetězec je true';
}

if (324)
{
    echo 'Nenulové číslo je true';
}

/*
 * Operátory
 * ||    OR
 * &&    AND
 * !     NOT
 *
 * !=    nerovno
 * <>    nerovno
 * !==   nerovno (striktní)
 * <
 * >
 * <=
 * >=
 * <=> - Spaceship
 */
