<?php

use Nette\Forms\Form;

$form->onSuccess[] = function (Form $form) {
    $this->flashMessage('Údaje byly upraveny', 'success');
    $this->redirectAjax();
};