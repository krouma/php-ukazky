<?php

class SimpleClass
{
    // property declaration
    private $var = 'a default value';

    /**
     * @return string
     */
    public function getVar(): string
    {
        return $this->var;
    }

    /**
     * @param string $var
     */
    public function setVar(string $var): void
    {
        $this->var = $var;
    }

    // method declaration
    public function displayVar(): void
    {
        echo $this->var;
    }
}

$simpleObject = new SimpleClass();
$simpleObject->displayVar();