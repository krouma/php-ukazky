<?php
/**
 * Created by PhpStorm.
 * User: krouma
 * Date: 28.3.19
 * Time: 17:31
 */

$i = 0;
while ($i < 10)
{
    echo "<li>Položka $i</li>";
    $i++;
}

for ($j = 0; $j < 10; $j++)
{
    echo "<li>Položka $j</li>";
}

$seznam = [
    'Jára' => 'Roth',
    'Ondra' => 'Rychtera',
    'Lukáš' => 'Drabik',
    'Adam' => 'Petr'
];

foreach ($seznam as $jmeno => $prijmeni)
{
    echo "Jméno: $jmeno";
    echo "Příjmení: $prijmeni";
}