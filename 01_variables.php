<?php
/**
 * Created by PhpStorm.
 * User: krouma
 * Date: 28.3.19
 * Time: 16:14
 */

$a = 'String $a';               // Všechny proměnné začínají znakem $
$b = 10;                        // Přiřazení
$c = 0.2;                       // Čísla s desetinným rozvojem
$d = "Počet: $b";               // Vložení obsahu proměnné do řetězce

$e = $b + $c;                   // Sčítání proměnných
$f = $b . $c;                   // Spojení proměnných

$g = $b++;                      // Přiřazení hodny z $b do $g, potom se $b zvětší o 1
$h = ++$b;                      // Zvětšení $b o 1, potom se $b přiřadí do $h

$b += 50;                       // Přičtení 50 k $b

/*
 * Další operátory
 * -    odečítání
 * *    násobení
 * /    dělení
 * %    modulo
 * **   exponent
 * --   zmenšení o 1
 */

// Pole (Array)
$pole = [1, 3, 4];
$pole2 = array(2, 4, 5);

$pole[] = 4;                    // Přidání do pole
$pole[] = ['a', 'b'];           // Přidání pole do pole

echo $pole[3];

// Asociativní pole
$pole3 = [
    'Jan' => 'Novák',
    40 => 50,
    true => false
];

echo $pole3['Jan'];