<?php

try {
    $a = 9/0;
} catch (DivisionByZeroError $exception) {
    echo 'Dělení nulou!';
    echo $exception->getMessage();
}

throw new HttpRequestException(); // Vyvolání výjimky
